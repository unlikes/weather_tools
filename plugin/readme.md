# 安装必备软件
1. pycharm
2. ananconda


#conda 添加清华源

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/

### conda-forge 第三方源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
### msys2 第三方源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/
### bioconda第三方源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/
### menpo 第三方源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/menpo/

conda config --set show_channel_urls yes


# 安装conda 中的python2.7环境


conda create --name python27 python=2.7

# 安装必要的包内容, 安装python27的basemap支持环境

conda install PIL numpy scipy matplotlib basemap


# 进入conda env环境

activate basemap27

# 退出conda env环境
deactivate python27

#查看当前python环境
python --version

#查看所有conda环境
conda info --envs


# 删除conda env环境

conda remove -n *环境名* --all





#安装3.0支持环境
conda install -c conda-forge basemap=1.0.8.dev0
conda install PIL numpy scipy matplotlib pyshp geopy

# 安装web支持环境

conda install sqlalchemy pyshp basemap iris