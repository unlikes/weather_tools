# 安装conda环境


conda create --name python27 python=2.7

# 安装必要的包内容, 安装python27的basemap支持环境

conda create --name python27 python=2.7

# 进入conda环境

activate basemap27

# conda install PIL numpy scipy matplotlib



#conda 添加源

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/


conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/menpo/

conda config --set show_channel_urls yes

#安装3.0支持环境
conda install -c conda-forge basemap=1.0.8.dev0
conda install PIL numpy scipy matplotlib pyshp

# 安装web支持环境

conda install sqlalchemy pyshp basemap iris