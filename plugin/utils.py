from matplotlib import colors
import os
import json
from PIL import Image
import time


def get_nc_file(path):

    # 用于读取某个目录下的所有nc文件, 并且排除掉sites文件,
    file_dict={}
    for (root, dirs, files) in os.walk(path):
        for filename in files:
            if valid_file_withsites(filename):
                file_dict[filename] = os.path.join(root, filename).replace("\\","/")
    return file_dict


# def get_cmp():
#     # 颜色调整函数 用于调整颜色
#     colorslist = ['#81B4F5',
#                   '#7EFC7B',
#                   '#E5FB8B',
#                   '#E77C74',
#                   '#B97CB9',
#                   "#9932CC",
#                   ]  # 该列表用于描述颜色序列, 也就是色度表
#     cmap = colors.LinearSegmentedColormap.from_list('mylist',
#                                                     colorslist,
#                                                     N=140,
#                                                     gamma=1.5)  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
#     return cmap


def valid_file_withsites(filename):
    # 用于排除文件中没有_Grid_的文件名, 防止处理_site_文件
    if "_Grid_" in filename:
        return True
    else:
        pass

# print(get_nc_file("data").keys())






def save(data, path):
    """
    data 是 dict 或者 list
    path 是保存文件的路径
    """
    s = json.dumps(data, indent=2, ensure_ascii=False)
    with open(path, 'w+', encoding='utf-8') as f:
        log('save', path, s, data)
        f.write(s)


def load(path):
    with open(path, 'r', encoding='utf-8') as f:
        s = f.read()
        log('load', s)
        return json.loads(s)

def pic_diejia():
    imA = Image.open('static/img/weather_tmp/blank1.png');
    imB = Image.open('static/img/weather_cache/sample_444.png');
    imC = Image.alpha_composite(imB, imA);
    imC.save('static/img/weather_img/sample_4446.png');

def pic_diejia_times(name,times):
    imA = Image.open('static/img/weather_tmp/blank1.png');
    imB = Image.open('static/img/weather_cache/sample_{}_{}.png'.format(name,times))
    imC = Image.alpha_composite(imB, imA);
    imC.save('static/img/weather_img/sample_{}_{}.png'.format(name,times))



def log(*args, **kwargs):
    # time.time() 返回 unix time
    # 如何把 unix time 转换为普通人类可以看懂的格式呢？
    format = '%H:%M:%S'
    value = time.localtime(int(time.time()))
    # 1500057448 -> 7:38:38
    dt = time.strftime(format, value)
    print(dt, *args, **kwargs)
    # a append 追加模式
    with open('log.gua.txt', 'a', encoding='utf-8') as f:
        print(dt, *args, file=f, **kwargs)


def formatted_time(unixtime):
    dt = time.localtime(unixtime)
    ds = time.strftime('%Y-%m-%d %H:%M:%S', dt)
    return ds


def make_dir(path):
    if os.path.exists(path):
        pass
    else:
        os.makedirs(path)


