from plugin.prototype import WeatherToken
import csv


class Citys():

    def geodistance(self, lng1, lat1, lng2, lat2):
        from math import radians, cos, sin, asin, sqrt
        lng1, lat1, lng2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
        dlon = lng2 - lng1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        dis = 2 * asin(sqrt(a)) * 6371 * 1000
        return dis

    def get_varunits_fromlatlon(self, lat_t, lon_t):
        # 取出 wathertoken 的内容用于提取资料.
        lat = lat_t
        for i in range(len(self.lats) - 1):
            if (lat - float(self.lats[i])) == 0:
                lat_i = i
            if (float(self.lats[i]) - lat) < 0 and (float(self.lats[i + 1]) - lat) > 0:
                lat_i = i
        lon = lon_t
        for i in range(len(self.lons) - 1):
            if (lon - float(self.lons[i])) == 0:
                lon_i = i
            if (float(self.lons[i]) - lon) < 0 and (float(self.lons[i + 1]) - lon) > 0:
                lon_i = i
        print("=========", (self.lats[lat_i], self.lons[lon_i]))
        lats = float(self.lats[lat_i])
        lons = float(self.lons[lon_i])
        # 用于找出最近的一点
        formlist = [(float(self.lats[lat_i]), float(self.lons[lon_i])),
                    (float(self.lats[lat_i + 1]), float(self.lons[lon_i])),
                    (float(self.lats[lat_i]), float(self.lons[lon_i + 1])),
                    (float(self.lats[lat_i + 1]), float(self.lons[lon_i + 1])),
                    ]
        legth_list = []
        for lat, lon in formlist:
            m = self.geodistance(lon_t, lat_t, lon, lat)
            legth_list.append(m)

        xuhao = legth_list.index(min(legth_list))
        print(xuhao)

        m = self.average_varunits(self.vars_units)
        print(lon_i)

        y = lon_t
        x = lat_t
        x1, y1, z11 = float(self.lats[lat_i]), float(self.lons[lon_i]), m[lat_i, lon_i]
        x2, y2, z21 = float(self.lats[lat_i + 1]), float(self.lons[lon_i + 1]), m[lat_i + 1, lon_i]
        x1, y2, z12 = float(self.lats[lat_i]), float(self.lons[lon_i + 1]), m[lat_i, lon_i + 1]
        x2, y1, z22 = float(self.lats[lat_i + 1]), float(self.lons[lon_i]), m[lat_i + 1, lon_i + 1]

        R1 = ((x2 - lat_t) / (x2 - x1)) * z11 + ((lat_t - x1) / (x2 - x1)) * z21
        R2 = ((x2 - lat_t) / (x2 - x1)) * z12 + ((lat_t - x1) / (x2 - x1)) * z22

        R = ((y2 - y) / (y2 - y1)) * R1 + ((y - y1) / (y2 - y1)) * R2

        print(z11, z12, z21, z22)

        return R

    def get_citys(self):
        csv_file = csv.reader(open('data_support/cityidloc.csv', 'r', encoding='utf-8'))
        self.city = []
        for city in csv_file:
            self.city.append(city)
        return self.city

    def city_name(self, lon, lat):
        citylist = self.get_citys()
        distance = []
        for city in citylist:

            test_lon = float(city[4])
            test_lat = float(city[5])
            distant = self.geodistance(lon, lat, test_lon, test_lat)
            distance.append(distant)


        minvaule = min(distance)
        print(minvaule)
        minindex = distance.index(minvaule)
        cityname = citylist[minindex]
        return cityname



