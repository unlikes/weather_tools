import shapefile
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from mpl_toolkits.basemap import Basemap

class Shape():
    def __init__(self,
                 shapefile_path =" data_support/geography_reference/country/country1"
                 ):
        self.sf = shapefile.Reader(shapefile_path, drawbounds=False)
        self.m =  Basemap(projection='merc')

    def get_shapepoints(self,leaf):
        for shape_rec in self.sf.shapeRecords():
            if shape_rec.record[3] == 'China':
                vertices = []
                codes = []
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append(self.m(pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
                # clip = PathPatch(clip, transform=self.ax.transData)
        return clip

