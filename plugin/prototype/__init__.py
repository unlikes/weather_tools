from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from models.weather_query import Weather_query
from mpl_toolkits.basemap import Basemap
import json,os
from plugin.utils import make_dir
from utils import log

from PIL import Image
import shapefile


def valid_file_withsites(filename):
    # 用于排除文件中没有_Grid_的文件名, 防止处理_site_文件
    if "_Grid_" in filename:
        return True
    else:
        pass


class WeatherToken():
    def __init__(self,
                 query_name,
                 lon_0,
                 lat_0,
                 llcrnrlon,
                 urcrnrlon,
                 llcrnrlat,
                 urcrnrlat,
                 projection,
                 cmp_list,
                 fh_file_path,
                 fh_vars_name,
                 cmp_n,
                 cmp_gamma,
                 pic_region,
                 ):

        # 初始化路径及需要查询的变量名
        self.query_name = query_name
        self.lon_0 = lon_0
        self.lat_0 = lat_0
        self.llcrnrlon = llcrnrlon
        self.urcrnrlon = urcrnrlon
        self.llcrnrlat = llcrnrlat
        self.urcrnrlat = urcrnrlat
        self.projection = projection
        self.cmp_list = cmp_list
        self.fh_file_path = fh_file_path
        self.vars_name = fh_vars_name
        self.cmp_n = cmp_n
        self.cmp_gamma = cmp_gamma
        self.region = pic_region
        self.savepath = ''
        self.vars = []

        # 打开nc文件
        fh = Dataset(self.fh_file_path, mode='r')

        # 获取nc文件中一维变量值
        self.lons = fh.variables['lon'][:]
        self.lats = fh.variables['lat'][:]
        self.times = fh.variables['time'][:]

        # 获取nc文件中所有经纬度的平均值
        # self.lon_0 = self.lons.mean()
        # self.lat_0 = self.lats.mean()

        # 获取nc文件中目标变量值
        self.vars_units = fh.variables[self.vars_name][:]
        self.vars = list(fh.variables.keys())
        fh.close()

    def get_cutpath(self, shapefile_path="data_support/geography_reference/country/country1"):
        # 用于修改切割的path的内容,默认是中国

        # 读取shapefile文件
        sf = shapefile.Reader(shapefile_path, drawbounds=False)

        # 提取path至clip
        for shape_rec in sf.shapeRecords():
            if shape_rec.record[3] == 'China':
                vertices = []
                codes = []
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append(self.m(pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
                clip = PathPatch(clip, transform=self.ax.transData)
        return clip

    def open_ncfile(self):
        # 打开nc文件
        fh = Dataset(self.file_path, mode='r')

        # 获取nc文件中一维变量值
        self.lons = fh.variables['lon'][:]
        self.lats = fh.variables['lat'][:]
        self.times = fh.variables['time'][:]

        # 获取nc文件中目标变量值
        self.vars_units = fh.variables[self.vars_name][:]

        # 关闭nc文件
        fh.close()

        # 返回经纬度,时间,目标变量
        # return self.lons, self.lats, self.times, self.vars_units

    def set_map(self):
        self.m = Basemap(lat_0=self.lat_0,
                         lon_0=self.lon_0,
                         resolution='l',
                         area_thresh=10000,
                         projection='merc',
                         llcrnrlon=70,
                         urcrnrlon=140,
                         llcrnrlat=16,
                         urcrnrlat=55
                         )

    def get_cmp(self):
        # 颜色调整函数 用于调整颜色
        log("======cmpn======", self.cmp_n)
        colorslist = self.cmp_list  # 该列表用于描述颜色序列, 也就是色度表
        cmap = colors.LinearSegmentedColormap.from_list('mylist',
                                                        colorslist,
                                                        N=self.cmp_n,
                                                        gamma=self.cmp_gamma
                                                        )  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
        return cmap

    def get_meshgrid_origin(self):
        lon, lat = np.meshgrid(self.lons, self.lats)
        xi, yi = self.m(lon, lat)
        return xi, yi

    def get_meshgrid_linespace(self):
        log('=======lons=====', len(self.lats))
        lon = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        lat = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)
        lon, lat = np.meshgrid(lon, lat)
        xi, yi = self.m(lon, lat)
        return xi, yi

    def build_basemap(self):
        # 这是为了满足远程的兼容性的结果
        plt.switch_backend('agg')
        self.fig = plt.figure(figsize=(14.03, 10.33), dpi=100, facecolor=None)
        self.ax = self.fig.add_subplot(111)
        return self.fig, self.ax

    def set_touming(self):
        self.fig.set_alpha(1)
        self.fig.set_facecolor('none')
        self.ax.set_alpha(1)
        self.ax.set_facecolor('none')
        self.ax.spines['top'].set_visible(False)
        self.ax.spines['right'].set_visible(False)
        self.ax.spines['bottom'].set_visible(False)
        self.ax.spines['left'].set_visible(False)
        return self.fig, self.ax

    def get_savename(self):
        print(self.fh_file_path)
        name = self.fh_file_path.split('/')[-1]
        return name

    def cut_pic(self, cachepath, savepath):
        img = Image.open(cachepath)
        region = self.region
        print(self.region)
        # 裁切图片
        cropImg = img.crop(region)

        # img = cropImg.convert('RGBA')
        # img_blender = Image.new('RGBA',img.size,(0, 0, 0, 0))
        # cropImg = Image.blend(img_blender, img, 0.8)

        # 保存裁切后的图片
        cropImg.save(savepath)

    def save_query(self, name, savepath):
        self.savepath = savepath
        self.query_name = name

        cmp_list = json.dumps(self.cmp_list, indent=2, ensure_ascii=False)
        region = json.dumps(self.region, indent=2, ensure_ascii=False)

        savedict = {
            'query_name': self.query_name,
            'lon_0': self.lon_0,
            'lat_0': self.lat_0,
            'llcrnrlon': self.llcrnrlon,
            'urcrnrlon': self.urcrnrlon,
            'llcrnrlat': self.llcrnrlat,
            'urcrnrlat': self.urcrnrlat,
            "projection": self.projection,
            'cmp_list': cmp_list,
            'fh_file_path': self.fh_file_path,
            'fh_vars_name': self.vars_name,
            'cmp_n': self.cmp_n,
            'cmp_gamma': self.cmp_gamma,
            'pic_region': region,
            'file_savepath': self.savepath,
        }
        if Weather_query.find_by(fh_vars_name=savedict['fh_vars_name'],fh_file_path=savedict['fh_file_path']) == []:
            Weather_query().new(savedict)



    def geodistance(self, lng1, lat1, lng2, lat2):
        from math import radians, cos, sin, asin, sqrt
        lng1, lat1, lng2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
        dlon = lng2 - lng1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        dis = 2 * asin(sqrt(a)) * 6371 * 1000
        return dis

    def get_varunits_fromlatlon(self, lat_t, lon_t):
        # 取出 wathertoken 的内容用于提取资料.
        lat = lat_t

        for i in range(len(self.lats) - 1):
            if (lat - float(self.lats[i])) == 0:
                lat_i = i
            if (float(self.lats[i]) - lat) < 0 and (float(self.lats[i + 1]) - lat) > 0:
                lat_i = i

        lon = lon_t

        for i in range(len(self.lons) - 1):
            if (lon - float(self.lons[i])) == 0:
                lon_i = i
            if (float(self.lons[i]) - lon) < 0 and (float(self.lons[i + 1]) - lon) > 0:
                lon_i = i
        print("=========", (self.lats[lat_i], self.lons[lon_i]))
        lats = float(self.lats[lat_i])
        lons = float(self.lons[lon_i])

        # 用于找出最近的一点
        formlist = [(float(self.lats[lat_i]), float(self.lons[lon_i])),
                    (float(self.lats[lat_i + 1]), float(self.lons[lon_i])),
                    (float(self.lats[lat_i]), float(self.lons[lon_i + 1])),
                    (float(self.lats[lat_i + 1]), float(self.lons[lon_i + 1])),
                    ]
        legth_list = []
        for lat, lon in formlist:
            m = self.geodistance(lon_t, lat_t, lon, lat)
            legth_list.append(m)

        xuhao = legth_list.index(min(legth_list))
        print(xuhao)

        m = self.average_varunits(self.vars_units)
        print(lon_i)

        y = lon_t
        x = lat_t
        x1, y1, z11 = float(self.lats[lat_i]), float(self.lons[lon_i]), m[lat_i,lon_i]
        x2, y2, z21 = float(self.lats[lat_i + 1]), float(self.lons[lon_i+1]), m[lat_i+1,lon_i]
        x1, y2, z12 = float(self.lats[lat_i]), float(self.lons[lon_i + 1]), m[lat_i,lon_i+1]
        x2, y1, z22 = float(self.lats[lat_i + 1]), float(self.lons[lon_i]), m[lat_i+1,lon_i+1]

        R1 = ((x2-lat_t)/(x2-x1))*z11+((lat_t-x1)/(x2-x1))*z21
        R2 = ((x2-lat_t)/(x2-x1))*z12+((lat_t-x1)/(x2-x1))*z22

        R = ((y2-y)/(y2-y1))*R1+ ((y-y1)/(y2-y1))*R2

        print(z11,z12,z21,z22)

        return R

    @classmethod
    def get_nc_file(cls,path):
        # 用于读取某个目录下的所有nc文件, 并且排除掉sites文件,
        file_dict = {}
        for (root, dirs, files) in os.walk(path):
            for filename in files:
                if valid_file_withsites(filename):
                    file_dict[filename] = os.path.join(root, filename).replace("\\", "/")
        return file_dict

    @classmethod
    def get_savepath(cls,filename,varunits='None',extends="default"):
        # 案例AQI_Grid_2017122420.nc
        n_times = filename.split('_')[-1].replace('.nc','')
        n1 = filename.replace('.nc','')
        make_dir('static/cache/{}/{}'.format(n_times,varunits))
        make_dir('static/img/{}/{}'.format(n_times, varunits))
        cachepath = 'static/cache/{}/{}/{}.png'.format(n_times,varunits,extends)
        savepath = 'static/img/{}/{}/{}.png'.format(n_times, varunits, extends)
        return cachepath,savepath


