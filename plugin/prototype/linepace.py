from plugin.prototype import WeatherToken
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from models.weather_query import Weather_query
from mpl_toolkits.basemap import Basemap
import json
from plugin.utils import make_dir
from utils import log
from scipy import interpolate

from PIL import Image
import shapefile


class Linspace(WeatherToken):



    def get_cmp_linespace(self):
        # 颜色调整函数 用于调整颜色
        colorslist = self.cmp_list  # 该列表用于描述颜色序列, 也就是色度表
        cmap = colors.LinearSegmentedColormap.from_list('mylist',
                                                        colorslist,
                                                        )  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
        return cmap




    def get_meshgrid_linespace(self):
        lon, lat = np.meshgrid(self.lons, self.lats)
        return lon, lat




    def set_color_average(self):
        # 生成网格矩阵
        xi, yi = self.get_meshgrid_origin()

        # 生成用于填充contourf的矩阵信息
        times_average = self.average_varunits(self.vars_units)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp()

        # print(np.squeeze(times_0))
        cs = self.m.contourf(xi, yi, np.squeeze(times_average), cmap=cmps)

        return cs


    def linespace_average_xiyiunits(self):
        # 生成网格矩阵
        lon, lat = self.lons , self.lats

        # m =  self.vars_units[0,:,:,]
        # log("===dfadsfsa========", self.vars_units[0,:,:,])
        # vars_units = np.squeeze(m)
        m = self.average_varunits(self.vars_units)

        # m = np.squeeze(self.vars_units[3,:,:])

        f = interpolate.interp2d(lon, lat, m)

        # lon_new = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        # lat_new = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)

        lon_new = np.linspace(self.lons[0], self.lons[-1], 1405, endpoint=True)
        lat_new = np.linspace(self.lats[0], self.lats[-1], 805, endpoint=True)


        vars_units_new = f(lon_new,lat_new)

        lon_new, lat_new = np.meshgrid(lon_new, lat_new)
        xi, yi = self.m(lon_new, lat_new)

        return xi,yi,vars_units_new


    def linespace_time_xiyiunits(self,time):
        # 生成网格矩阵
        lon, lat = self.lons , self.lats

        # m =  self.vars_units[0,:,:,]
        # log("===dfadsfsa========", self.vars_units[0,:,:,])
        # vars_units = np.squeeze(m)
        # m = self.average_varunits(self.vars_units)

        m = np.squeeze(self.time_varsunits(self.vars_units,time))

        f = interpolate.interp2d(lon, lat, m)

        # lon_new = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        # lat_new = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)

        lon_new = np.linspace(self.lons[0], self.lons[-1], 1405, endpoint=True)
        lat_new = np.linspace(self.lats[0], self.lats[-1], 805, endpoint=True)


        vars_units_new = f(lon_new,lat_new)

        lon_new, lat_new = np.meshgrid(lon_new, lat_new)
        xi, yi = self.m(lon_new, lat_new)

        return xi,yi,vars_units_new


    def set_color_average_linspace(self):
        log("========succeed======")


        # 生成用于填充contourf的矩阵信息

        xi,yi,vars_units_new = self.linespace_average_xiyiunits()

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp_linespace()

        # print(np.squeeze(times_0))
        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new), cmap=cmps, alpha=0.9)


        cs = self.m.contourf(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)
        # cs =  self.m.pcolor(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)

        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new))

        return cs


    def set_color_time_linspace(self,time):
        log("========succeed======")


        # 生成用于填充contourf的矩阵信息

        xi,yi,vars_units_new = self.linespace_time_xiyiunits(time)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp_linespace()

        # print(np.squeeze(times_0))
        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new), cmap=cmps, alpha=0.9)


        cs = self.m.contourf(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)
        # cs =  self.m.pcolor(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)

        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new))

        return cs




    def average_varunits(self, vars_units):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        # times_0 = vars_units[:, :, :, ]

        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        dots = 1 / 217
        times_average = np.dot(times_sum, dots)

        return times_average







    def draw_by_average_linespace(self):

        # 经纬度平均值
        # matplotlib 的基本制图初始化操作
        self.build_basemap()
        # 设置地图透明化
        self.set_touming()
        # 初始化basemap
        self.set_map()

        # print(np.squeeze(times_0))
        cs = self.set_color_average_linspace()

        # clips
        # 导入shapefile文件
        clip = self.get_cutpath()

        # 等值线的导出类型CS中包含collections，可以直接通过其中的set_clip_path函数来切除边缘，不过只限于contour等值线层，
        # 详细参见http: // basemaptutorial.readthedocs.io / en / latest / clip.html

        for contour in cs.collections:
            contour.set_clip_path(clip)

        # set colourbar
        cbar = self.m.colorbar(cs, location='bottom', pad="10%")

        name = self.get_savename()

        make_dir("static/img/weather_cache/{}".format(name))
        make_dir("static/img/weather_img/{}".format(name))
        cachepath ="static/img/weather_cache/{}/{}_{}_average.png".format(name, name,self.vars_name)
        savepath = "static/img/weather_img/{}/{}_{}_average.png".format(name, name,self.vars_name)
        # 保存文件至weather_img文件夹
        log("======================")
        plt.savefig(cachepath, dpi=500, transparent=True)
        self.cut_pic(cachepath,savepath)
        self.save_query(name,savepath)
        # plt.show()
        plt.close()




