from plugin.prototype import WeatherToken


import numpy as np
import matplotlib.pyplot as plt

class Bytimes(WeatherToken):


    def time_varsunits(self, vars_units, times):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        times_0 = vars_units[times, :, :, ]

        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        # times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        # dots = 1 / 217
        # times_average = np.dot(times_sum, dots)
        return times_0

    def set_color_by_times(self, times):
        # 生成网格矩阵
        xi, yi = self.get_meshgrid_origin()

        # 生成用于填充contourf的矩阵信息
        times_average = self.time_varsunits(self.vars_units, times)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp()

        # print(np.squeeze(times_0))
        cs = self.m.contourf(xi, yi, np.squeeze(times_average), cmap=cmps, alpha=0.9)

        return cs


    def draw_by_time(self):
        for x in self.times:

            # 经纬度平均值
            # matplotlib 的基本制图初始化操作
            self.build_basemap()
            self.set_touming()
            # 获取地图中心点
            # lon_0 , lat_0 = self.get_meanlonlat(self.lons, self.lats)
            self.set_map()

            # print(np.squeeze(times_0))
            cs = self.set_color_by_times(x)

            # clips
            # 导入shapefile文件
            clip = self.get_cutpath()

            # 等值线的导出类型CS中包含collections，可以直接通过其中的set_clip_path函数来切除边缘，不过只限于contour等值线层，
            # 详细参见http: // basemaptutorial.readthedocs.io / en / latest / clip.html

            for contour in cs.collections:
                contour.set_clip_path(clip)

            name = self.get_savename()
            cachepath, savepath = self.get_savepath(name, self.vars_name)
            # 保存文件至weather_img文件夹
            plt.savefig(cachepath, dpi=100, transparent=True)
            self.cut_pic(cachepath, savepath)
            self.save_query(name, savepath)
            # plt.show()
            plt.close()