from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from models.weather_query import Weather_query
from mpl_toolkits.basemap import Basemap
import json
from plugin.utils import make_dir
from utils import log
from scipy import interpolate

from PIL import Image
import shapefile


class WeatherToken( ):
    def __init__(self,
                 query_name,
                 lon_0,
                 lat_0,
                 llcrnrlon,
                 urcrnrlon,
                 llcrnrlat,
                 urcrnrlat,
                 projection,
                 cmp_list,
                 fh_file_path,
                 fh_vars_name,
                 cmp_n,
                 cmp_gamma,
                 pic_region,
                 ):

        # 初始化路径及需要查询的变量名
        self.query_name =  query_name
        self.lon_0 =lon_0
        self.lat_0 = lat_0
        self.llcrnrlon = llcrnrlon
        self.urcrnrlon = urcrnrlon
        self.llcrnrlat = llcrnrlat
        self.urcrnrlat = urcrnrlat
        self.projection = projection
        self.cmp_list = cmp_list
        self.fh_file_path = fh_file_path
        self.vars_name = fh_vars_name
        self.cmp_n = cmp_n
        self.cmp_gamma = cmp_gamma
        self.region = pic_region
        self.query_name = ''
        self.savepath = ''

        # 打开nc文件
        fh = Dataset(self.fh_file_path, mode='r')

        # 获取nc文件中一维变量值
        self.lons = fh.variables['lon'][:]
        self.lats = fh.variables['lat'][:]
        self.times = fh.variables['time'][:]

        # 获取nc文件中所有经纬度的平均值
        # self.lon_0 = self.lons.mean()
        # self.lat_0 = self.lats.mean()

        # 获取nc文件中目标变量值
        self.vars_units = fh.variables[self.vars_name][:]

        # 关闭nc文件
        fh.close()

    def get_cutpath(self, shapefile_path="data_support/geography_reference/country/country1"):
        # 用于修改切割的path的内容,默认是中国

        # 读取shapefile文件
        sf = shapefile.Reader(shapefile_path, drawbounds=False)

        # 提取path至clip
        for shape_rec in sf.shapeRecords():
            log(shape_rec.record[3])
            if shape_rec.record[3] == 'China':
                vertices = []
                codes = []
                pts = shape_rec.shape.points
                prt = list(shape_rec.shape.parts) + [len(pts)]
                for i in range(len(prt) - 1):
                    for j in range(prt[i], prt[i + 1]):
                        vertices.append(self.m(pts[j][0], pts[j][1]))
                    codes += [Path.MOVETO]
                    codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                    codes += [Path.CLOSEPOLY]
                clip = Path(vertices, codes)
                clip = PathPatch(clip, transform=self.ax.transData)
        return clip

    def open_ncfile(self):
        # 打开nc文件
        fh = Dataset(self.file_path, mode='r')

        # 获取nc文件中一维变量值
        self.lons = fh.variables['lon'][:]
        self.lats = fh.variables['lat'][:]
        self.times = fh.variables['time'][:]

        # 获取nc文件中目标变量值
        self.vars_units = fh.variables[self.vars_name][:]

        # 关闭nc文件
        fh.close()

        # 返回经纬度,时间,目标变量
        # return self.lons, self.lats, self.times, self.vars_units

    def set_map(self):
        self.m = Basemap(lat_0=self.lat_0,
                         lon_0=self.lon_0,
                         resolution='l',
                         area_thresh=10000,
                         projection='merc',
                         llcrnrlon=70,
                         urcrnrlon=140,
                         llcrnrlat=16,
                         urcrnrlat=55
                         )

    def get_cmp(self):
        # 颜色调整函数 用于调整颜色
        colorslist = self.cmp_list  # 该列表用于描述颜色序列, 也就是色度表
        cmap = colors.LinearSegmentedColormap.from_list('mylist',
                                                        colorslist,
                                                        N=self.cmp_n,
                                                        gamma=self.cmp_gamma
                                                        )  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
        return cmap

    def get_cmp_linespace(self):
        # 颜色调整函数 用于调整颜色
        colorslist = self.cmp_list  # 该列表用于描述颜色序列, 也就是色度表
        cmap = colors.LinearSegmentedColormap.from_list('mylist',
                                                        colorslist,
                                                        )  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
        return cmap


    def get_meshgrid_origin(self):
        lon, lat = np.meshgrid(self.lons, self.lats)
        xi, yi = self.m(lon, lat)
        log("==========x=========",xi)
        return xi,yi


    def get_meshgrid_linespace(self):
        lon, lat = np.meshgrid(self.lons, self.lats)
        return lon, lat


    def set_color_by_times(self, times):
        # 生成网格矩阵
        xi, yi = self.get_meshgrid_origin()
        lon = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        lat = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)

        vars_units = interpolate.interp2d(lon, lat, self.vars_units, kind = 'cubic')

        # 生成用于填充contourf的矩阵信息
        times_average = self.time_varsunits(vars_units, times)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp()

        # print(np.squeeze(times_0))
        cs = self.m.contourf(xi, yi, np.squeeze(times_average), cmap=cmps, alpha=0.9)

        return cs

    def set_color_average(self):
        # 生成网格矩阵
        xi, yi = self.get_meshgrid_origin()

        # 生成用于填充contourf的矩阵信息
        times_average = self.average_varunits(self.vars_units)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp()

        # print(np.squeeze(times_0))
        cs = self.m.contourf(xi, yi, np.squeeze(times_average), cmap=cmps)

        return cs


    def linespace_average_xiyiunits(self):
        # 生成网格矩阵
        lon, lat = self.lons , self.lats

        # m =  self.vars_units[0,:,:,]
        # log("===dfadsfsa========", self.vars_units[0,:,:,])
        # vars_units = np.squeeze(m)
        m = self.average_varunits(self.vars_units)

        # m = np.squeeze(self.vars_units[3,:,:])

        f = interpolate.interp2d(lon, lat, m)

        # lon_new = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        # lat_new = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)

        lon_new = np.linspace(self.lons[0], self.lons[-1], 1405, endpoint=True)
        lat_new = np.linspace(self.lats[0], self.lats[-1], 805, endpoint=True)


        vars_units_new = f(lon_new,lat_new)

        lon_new, lat_new = np.meshgrid(lon_new, lat_new)
        xi, yi = self.m(lon_new, lat_new)

        return xi,yi,vars_units_new


    def linespace_time_xiyiunits(self,time):
        # 生成网格矩阵
        lon, lat = self.lons , self.lats

        # m =  self.vars_units[0,:,:,]
        # log("===dfadsfsa========", self.vars_units[0,:,:,])
        # vars_units = np.squeeze(m)
        # m = self.average_varunits(self.vars_units)

        m = np.squeeze(self.time_varsunits(self.vars_units,time))

        f = interpolate.interp2d(lon, lat, m)

        # lon_new = np.linspace(self.lons[0], self.lons[-1], 843, endpoint=True)
        # lat_new = np.linspace(self.lats[0], self.lats[-1], 483, endpoint=True)

        lon_new = np.linspace(self.lons[0], self.lons[-1], 1405, endpoint=True)
        lat_new = np.linspace(self.lats[0], self.lats[-1], 805, endpoint=True)


        vars_units_new = f(lon_new,lat_new)

        lon_new, lat_new = np.meshgrid(lon_new, lat_new)
        xi, yi = self.m(lon_new, lat_new)

        return xi,yi,vars_units_new


    def set_color_average_linspace(self):
        log("========succeed======")


        # 生成用于填充contourf的矩阵信息

        xi,yi,vars_units_new = self.linespace_average_xiyiunits()

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp_linespace()

        # print(np.squeeze(times_0))
        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new), cmap=cmps, alpha=0.9)


        cs = self.m.contourf(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)
        # cs =  self.m.pcolor(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)

        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new))

        return cs


    def set_color_time_linspace(self,time):
        log("========succeed======")


        # 生成用于填充contourf的矩阵信息

        xi,yi,vars_units_new = self.linespace_time_xiyiunits(time)

        # 着色函数, 为contourf的cmap参数提供值
        cmps = self.get_cmp_linespace()

        # print(np.squeeze(times_0))
        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new), cmap=cmps, alpha=0.9)


        cs = self.m.contourf(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)
        # cs =  self.m.pcolor(xi, yi, vars_units_new, cmap=cmps, alpha=0.9)

        # cs = self.m.contourf(xi, yi, np.squeeze(vars_units_new))

        return cs




    def average_varunits(self, vars_units):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        # times_0 = vars_units[:, :, :, ]

        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        dots = 1 / 217
        times_average = np.dot(times_sum, dots)

        return times_average



    def time_varsunits(self, vars_units,times):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        times_0 = vars_units[times, :, :, ]


        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        # times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        # dots = 1 / 217
        # times_average = np.dot(times_sum, dots)
        return times_0


    def build_basemap(self):
        self.fig = plt.figure(figsize=(14.03, 10.33), dpi=100, facecolor=None)
        self.ax = self.fig.add_subplot(111)
        return self.fig, self.ax

    def set_touming(self):
        self.fig.set_alpha(1)
        self.fig.set_facecolor('none')
        self.ax.set_alpha(1)
        self.ax.set_facecolor('none')
        self.ax.spines['top'].set_visible(False)
        self.ax.spines['right'].set_visible(False)
        self.ax.spines['bottom'].set_visible(False)
        self.ax.spines['left'].set_visible(False)
        return self.fig, self.ax

    def get_savename(self):
        print(self.fh_file_path)
        filename = self.fh_file_path.split('/')[-1]
        filename =  filename.split('.')[0]
        name = filename.split('_')[2]
        return name

    def cut_pic(self,cachepath,savepath):
        img = Image.open(cachepath)
        region = self.region
        print(self.region)
        # 裁切图片
        cropImg = img.crop(region)

        # img = cropImg.convert('RGBA')
        # img_blender = Image.new('RGBA',img.size,(0, 0, 0, 0))
        # cropImg = Image.blend(img_blender, img, 0.8)

        # 保存裁切后的图片
        cropImg.save(savepath)

    def save_query(self, name, savepath):
        self.savepath = savepath
        self.query_name = name

        cmp_list =  json.dumps(self.cmp_list, indent=2, ensure_ascii=False)
        region = json.dumps(self.region, indent=2, ensure_ascii=False)

        savedict = {
            'query_name' : self.query_name,
            'lon_0' : self.lon_0,
            'lat_0' :self.lat_0,
            'llcrnrlon' : self.llcrnrlon,
            'urcrnrlon' : self.urcrnrlon,
            'llcrnrlat' : self.llcrnrlat,
            'urcrnrlat' : self.urcrnrlat,
            "projection": self.projection,
            'cmp_list': cmp_list,
            'fh_file_path': self.fh_file_path,
            'fh_vars_name' : self.vars_name,
            'cmp_n': self.cmp_n,
            'cmp_gamma' : self.cmp_gamma,
            'pic_region' : region,
            'file_savepath': self.savepath,
        }

        Weather_query().new(savedict)

    def draw_by_time(self):
        for x in self.times:

            # 经纬度平均值
            # matplotlib 的基本制图初始化操作
            self.build_basemap()
            self.set_touming()
            # 获取地图中心点
            # lon_0 , lat_0 = self.get_meanlonlat(self.lons, self.lats)
            self.set_map()

            # print(np.squeeze(times_0))
            cs = self.set_color_by_times(x)

            # clips
            # 导入shapefile文件
            clip = self.get_cutpath()

            # 等值线的导出类型CS中包含collections，可以直接通过其中的set_clip_path函数来切除边缘，不过只限于contour等值线层，
            # 详细参见http: // basemaptutorial.readthedocs.io / en / latest / clip.html

            for contour in cs.collections:
                contour.set_clip_path(clip)
            name = self.get_savename()
            cachepath = "static/img/weather_cache/{}/{}_{}_{}.png".format(name, name, self.vars_name,x)
            savepath = "static/img/weather_img/{}/{}_{}_{}.png".format(name, name, self.vars_name, x)
            # 保存文件至weather_img文件夹
            plt.savefig(cachepath, dpi=100, transparent=True)
            self.cut_pic(cachepath,savepath)
            self.save_query(name,savepath)
            # plt.show()
            plt.close()

    def draw_by_average_linespace(self):

        # 经纬度平均值
        # matplotlib 的基本制图初始化操作
        self.build_basemap()
        # 设置地图透明化
        self.set_touming()
        # 初始化basemap
        self.set_map()

        # print(np.squeeze(times_0))
        cs = self.set_color_average_linspace()

        # clips
        # 导入shapefile文件
        clip = self.get_cutpath()

        # 等值线的导出类型CS中包含collections，可以直接通过其中的set_clip_path函数来切除边缘，不过只限于contour等值线层，
        # 详细参见http: // basemaptutorial.readthedocs.io / en / latest / clip.html

        for contour in cs.collections:
            contour.set_clip_path(clip)

        # set colourbar
        cbar = self.m.colorbar(cs, location='bottom', pad="10%")

        name = self.get_savename()

        make_dir("static/img/weather_cache/{}".format(name))
        make_dir("static/img/weather_img/{}".format(name))
        cachepath ="static/img/weather_cache/{}/{}_{}_average.png".format(name, name,self.vars_name)
        savepath = "static/img/weather_img/{}/{}_{}_average.png".format(name, name,self.vars_name)
        # 保存文件至weather_img文件夹
        log("======================")
        plt.savefig(cachepath, dpi=500, transparent=True)
        self.cut_pic(cachepath,savepath)
        self.save_query(name,savepath)
        # plt.show()
        plt.close()




