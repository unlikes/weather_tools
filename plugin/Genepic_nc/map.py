from plugin.Genepic_nc import Ncreader
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from models.weather_query import Weather_query
from mpl_toolkits.basemap import Basemap
import json
from plugin.utils import make_dir
from utils import log

from PIL import Image
import shapefile



class Map():
    def __init__(self, root_dict):
        # 投影方法
        self.projection = root_dict['projection']

        # 边界集合
        # todo 这里需要在model里增加一个属性, 用一个列表说明哪些省份作为绘制边界
        # todo 需要完成border函数, 用于说明剪切边界
        bordersleaf = root_dict.get('Provence')
        self.borders = []
        for borderleaf in bordersleaf:
            # self.borders.append(Border(borderleaf))
            pass

        # 剪切集合
        # clip区域集合
        # todo 需要增加一个Provence_cut参数 同上在model里增加

        clipsleaf = root_dict.get['Provence_cut']
        self.clipborders = []
        for clipleaf in clipsleaf:
            pass
            # self.clipborders.append(ClipBorder(clipleaf))

        # 站点文件
        # todo 需要增加站点文件的读取方法,以及读取说明


    def BuildClassicMap(self):
        self.fig = plt.figure(figsize=(14.03, 10.33), dpi=100, facecolor=None)
        self.ax = self.fig.add_subplot(111)
        self.fig.set_alpha(1)
        self.fig.set_facecolor('none')
        self.ax.set_alpha(1)
        self.ax.set_facecolor('none')
        self.ax.spines['top'].set_visible(False)
        self.ax.spimapnes['right'].set_visible(False)
        self.ax.spines['bottom'].set_visible(False)
        self.ax.spines['left'].set_visible(False)
        map = Basemap(lat_0=self.lat_0,
                           lon_0=self.lon_0,
                           resolution='l',
                           area_thresh=10000,
                           projection='merc',
                           llcrnrlon=70,
                           urcrnrlon=140,
                           llcrnrlat=16,
                           urcrnrlat=55
                           )
        return map


    def Buildchosemap(self,
                      mirror=False,
                      lat_0=105,
                      lon_0=35,
                      resolution='l',
                      area_thresh=10000,
                      projection='merc',
                      llcrnrlon=70,
                      urcrnrlon=140,
                      llcrnrlat=16,
                      urcrnrlat=55
                      ):
        self.fig = plt.figure(figsize=(14.03, 10.33), dpi=100, facecolor=None)
        self.ax = self.fig.add_subplot(111)
        if mirror:
            self.fig.set_alpha(1)
            self.fig.set_facecolor('none')
            self.ax.set_alpha(1)
            self.ax.set_facecolor('none')
            self.ax.spines['top'].set_visible(False)
            self.ax.spimapnes['right'].set_visible(False)
            self.ax.spines['bottom'].set_visible(False)
            self.ax.spines['left'].set_visible(False)

        self.map = Basemap(lat_0=lat_0,
                           lon_0=lon_0,
                           resolution=resolution,
                           area_thresh=area_thresh,
                           projection=projection,
                           llcrnrlon=llcrnrlon,
                           urcrnrlon=urcrnrlon,
                           llcrnrlat=llcrnrlat,
                           urcrnrlat=urcrnrlat
                           )
        return map



class Data2Color():
    def __init__(self, varunits, tag_words='PM10',projection='merc'):
        self.tag_words = tag_words
        self.vars_units = varunits
        self.m = Basemap(projection)

    def average_varunits(self, vars_units):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        # times_0 = vars_units[:, :, :, ]

        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        dots = 1 / 217
        times_average = np.dot(times_sum, dots)

        return times_average

    def time_varsunits(self, vars_units, times):
        # 获取所有维度下的vars_units的变量, 这里是待定的,
        times_0 = vars_units[times, :, :, ]

        # 求出每小时的平均值
        # 按时间维度合计vars_units的值
        # times_sum = np.sum(vars_units, axis=0)

        # 求平均值
        # dots = 1 / 217
        # times_average = np.dot(times_sum, dots)
        return times_0

    def get_cmp(self):
        # 颜色调整函数 用于调整颜色
        log("======cmpn======", self.cmp_n)
        colorslist = self.cmp_list  # 该列表用于描述颜色序列, 也就是色度表
        cmap = colors.LinearSegmentedColormap.from_list('mylist',
                                                        colorslist,
                                                        N=self.cmp_n,
                                                        gamma=self.cmp_gamma
                                                        )  # N是指插入的相近颜色数量 gamma是指颜色偏移值,
        return cmap


class Draw():
    pass






