from netCDF4 import Dataset
import os,json
from utils import log


def valid_file_withsites(filename):
    # 用于排除文件中没有_Grid_的文件名, 防止处理_site_文件
    if "_Grid_" in filename:
        return True
    else:
        pass

def save(data, path):
    """
    data 是 dict 或者 list
    path 是保存文件的路径
    """
    s = json.dumps(data, indent=2, ensure_ascii=False)
    with open(path, 'w+', encoding='utf-8') as f:
        log('save', path, s, data)
        f.write(s)

def load(path):
    with open(path, 'r', encoding='utf-8') as f:
        s = f.read()
        log('load', s)
        return json.loads(s)







class Ncreader():
    def __init__(self,filepath):
        self.filepath = filepath


    def open_nc(self):
        # 打开nc文件
        fh = Dataset(self.fh_file_path, mode='r')

        # 获取nc文件中一维变量值
        self.lons = fh.variables['lon'][:]
        self.lats = fh.variables['lat'][:]
        self.times = fh.variables['time'][:]

        # 获取nc文件中所有经纬度的平均值
        # self.lon_0 = self.lons.mean()
        # self.lat_0 = self.lats.mean()

        # 获取nc文件中目标变量值
        self.vars_units = fh.variables[self.vars_name][:]

        # 关闭nc文件
        fh.close()


    @classmethod
    def get_nc_file(cls,path):
        # 用于读取某个目录下的所有nc文件, 并且排除掉sites文件,
        file_dict = {}
        for (root, dirs, files) in os.walk(path):
            for filename in files:
                if valid_file_withsites(filename):
                    file_dict[filename] = os.path.join(root, filename).replace("\\", "/")
        return file_dict

    @classmethod
    def get_savepath(cls,filename,varunits='None',extends="default"):
        # 案例AQI_Grid_2017122420.nc
        n_times = filename.split('_')[-1].replace('.nc','')
        n1 = filename.replace('.nc','')
        cachepath = 'cache/{}/{}/{}.png'.format(n_times,varunits,extends)
        savepath = 'img/{}/{}/{}.png'.format(n_times, varunits, extends)
        return cachepath,savepath








