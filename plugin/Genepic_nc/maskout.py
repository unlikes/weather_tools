# 用于对图形进行白化的函数

import shapefile
from matplotlib.path import Path
from matplotlib.patches import PathPatch


def get_cutpath( ax,
                 basemap_class,
                 shapefile_path="data_support/geography_reference/country/country1",
                ):
    # 用于修改切割的path的内容,默认是中国

    # 读取shapefile文件
    sf = shapefile.Reader(shapefile_path, drawbounds=False)

    # 提取path至clip
    for shape_rec in sf.shapeRecords():
        if shape_rec.record[3] == 'China':
            vertices = []
            codes = []
            pts = shape_rec.shape.points
            prt = list(shape_rec.shape.parts) + [len(pts)]
            for i in range(len(prt) - 1):
                for j in range(prt[i], prt[i + 1]):
                    vertices.append(basemap_class(pts[j][0], pts[j][1]))
                codes += [Path.MOVETO]
                codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
                codes += [Path.CLOSEPOLY]
            clip = Path(vertices, codes)
            clip = PathPatch(clip, transform= ax.transData)
    return clip