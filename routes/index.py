from flask import (
    render_template,
    request,
    Blueprint,
    abort,
)

from models.weather_query import Weather_query

from models.article import Article

from plugin.prototype.avarage import Average

main = Blueprint('index', __name__)


"""
用户在这里可以
    访问首页
    注册
    登录

用户登录后, 会写入 session, 并且定向到 /profile
"""


@main.route("/")
def index():
    Weather_query().creat_sheet()
    weatherdic={}
    dirdict = Average.get_nc_file('data_inbox')
    for filename, filepath in dirdict.items():
        weatherdic[filename]=Weather_query().find_by(query_name=filename)

    return render_template('weather_index.html',weather_query = weatherdic)




@main.route('/error')
def error():
    code = int(request.args.get('code'))
    abort(code)



