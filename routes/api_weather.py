from flask import (
    jsonify,
    Blueprint,
    request,
    redirect,
)

from utils import log
from models.weather_query import Weather_query
from plugin.prototype.citys import Citys
from plugin.prototype.avarage import Average

main = Blueprint('api_weather', __name__)


# 本文件只返回 json 格式的数据
# 而不是 html 格式的数据

@main.route('/GetCityName', methods=['GET'])
def delete():
    lon = float(request.args.get('lon'))
    lat = float(request.args.get('lat'))
    m = Citys()
    cityname = m.city_name(lon,lat)
    print(cityname)
    return jsonify(cityname)

@main.route('/creat_pic', methods=['GET'])
def creat_pic():
    commands = request.args.get('commands')
    workdict={}
    get_vas = ['PM25', 'PM10', 'SO2', 'NO2', 'CO', 'O3', 'AQI']
    for var in get_vas:
        workdict[var]=[]
    if commands == 'all':
        dirdict = Average.get_nc_file('data_inbox')
        for filename,filepath in dirdict.items():
            for var in get_vas:
                m = Average(filename,
                            105,
                            35,
                            70,
                            140,
                            16,
                            55,
                            'merc',
                            ['#81B4F5', '#7EFC7B', '#E5FB8B', '#E77C74', '#B97CB9', '#9932CC'],
                            filepath,
                            var,
                            256,
                            1,
                            (200, 170, 1180, 900)
                            )
                cacahepath,savepath = m.draw_by_average()
                workdict[var].append(savepath)

    return jsonify(workdict)



@main.route('/search_from_static', methods=['GET'])
def search_from_static():
    m = localize()
    return jsonify(m)

def localize():
    result={}
    cache = []
    search = Weather_query.all()
    for x in search:
        if x.query_name not in cache:
            cache.append(x.query_name)

    for x in cache:
        result[x]=[]
        for y in Weather_query.find_by(query_name=x):
            result[x].append(y.file_savepath)

    return result




@main.route('/creatpicbypath', methods=['GET'])
def creatpicbypath():
    path = request.args.get('path')
    workdict={}
    dirdict = Average.get_nc_file(path)
    for filename,filepath in dirdict.items():
        m = Average(filename,
                    105,
                    35,
                    70,
                    140,
                    16,
                    55,
                    'merc',
                    ['#81B4F5', '#7EFC7B', '#E5FB8B', '#E77C74', '#B97CB9', '#9932CC'],
                    filepath,
                    'PM10',
                    256,
                    1,
                    (200, 170, 1180, 900)
                    )

        cacahepath,savepath = m.draw_by_average()
        workdict[filename] = savepath
    return jsonify(workdict)