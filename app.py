#coding:utf-8
from flask import Flask
from flask import (
    render_template,
)
from routes.index import main as index_routes
from routes.api_weather import main as api_weather
from flaskext.markdown import Markdown
from config import secret_key





def configured_app():
    # web framework
    # web application
    # __main__
    app = Flask(__name__)
    Markdown(app, extensions=['extra', 'markdown.extensions.nl2br', 'wikilinks'], safe_mode=True)
    # 设置 secret_key 来使用 flask 自带的 session
    # 这个字符串随便你设置什么内容都可以
    app.secret_key = secret_key
    """
    在 flask 中，模块化路由的功能由 蓝图（Blueprints）提供
    蓝图可以拥有自己的静态资源路径、模板路径（现在还没涉及）
    用法如下
    """
    # 注册蓝图
    # 有一个 url_prefix 可以用来给蓝图中的每个路由加一个前缀
    app.register_blueprint(index_routes)
    app.register_blueprint(api_weather, url_prefix='/api_weather')

    #注册自有过滤器


    @app.errorhandler(404)
    def error(e):
        return render_template('404.html'), 404

    return app





# 运行代码
if __name__ == '__main__':
    # debug 模式可以自动加载你对代码的变动, 所以不用重启程序
    # host 参数指定为 '0.0.0.0' 可以让别的机器访问你的代码
    app = configured_app()
    # 自动 reload jinja
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.jinja_env.auto_reload = True
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    # debug 模式可以自动加载你对代码的变动, 所以不用重启程序
    # host 参数指定为 '0.0.0.0' 可以让别的机器访问你的代码
    config = dict(
        debug=True,
        host='0.0.0.0',
        port=3000,
        threaded=True,
    )
    app.run(**config)
