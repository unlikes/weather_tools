// TODO API
// 获取所有 article
var apiArticleAll = function(callback) {
    var path = `/api/article/all`
    ajax('GET', path, '', callback)
}

var apiArticleDelete = function(id, callback) {
    var path = `/api/article/delete?id=${id}`
    ajax('GET', path, '', callback)
}

// 增加一个 article
var apiArticleAdd = function(form, callback) {
    var path = `/article/add`
    ajax('POST', path, form, callback)
}

var apiArticleUpdate = function(form, callback) {
    var path = `/api/article/update`
    ajax('POST', path, form, callback)
}

// TODO DOM
var articleTemplate = function(article) {
    var t = `
        <div class="article-cell">
            <button data-id=${article.id} class="article-delete">删除</button>
            <button data-id=${article.id} class="article-edit">编辑</button>
            <span class="article-task" >${article.task}</span>
            <span>创建时间： ${article.created_time}</span>
            <span>修改时间： ${article.updated_time}</span>
        </div>
    `
    return t
}

var articleUpdateTemplate = function(article_id) {
    var t = `
        <div class="article-update-form">
            <input class="article-update-input">
            <button data-id=${article_id} class="article-update">更新</button>
        </div>
    `
    return t
}

//var insertArticle = function(article) {
//    var articleCell = articleTemplate(article)
//    // 插入 article-list
//    var articleList = e('.article-list')
//    articleList.insertAdjacentHTML('beforeend', articleCell)
//}
//
//var loadArticles = function() {
//    // 调用 ajax api 来载入数据
//    apiArticleAll(function(r) {
//        console.log('load all', r)
//        // 解析为 数组
//        var articles = JSON.parse(r)
//        // 循环添加到页面中
//        for(var i = 0; i < articles.length; i++) {
//            var article = articles[i]
//            insertArticle(article)
//        }
//    })
//}
//
var bindEventArticleAdd = function() {
    var b = e('.wysiwyg-btn')
    // 注意, 第二个参数可以直接给出定义函数
    b.addEventListener('click', function(){
        var article_src =  e('.wysiwyg').innerHTML
        var article = article_src.replace(/<div>/g,` /r/n `)
        var article = article.replace(/<\/div>/g,``)

        var title_src = e('.blog-title')
        var title = title_src.value


        // 下面的form都是以json形式发送出去的
        var form = {
            title : title,
            article: article,
        }
        log("form", form)
        apiArticleAdd(form, function(response) {
            // 收到返回的数据, 插入到页面中
        })
    })
}
//
//var bindEventArticleDelete = function() {
//    var articleList = e('.article-list')
//    // 事件响应函数会传入一个参数 就是事件本身
//    articleList.addEventListener('click', function (event) {
//        log('============event===========',event)
//        // 我们可以通过 event.target 来得到被点击的对象
//        var self = event.target
//        log('被点击的元素', self)
//        // 通过比较被点击元素的 class
//        // 来判断元素是否是我们想要的
//        // classList 属性保存了元素所有的 class
//        log(self.classList)
//        if (self.classList.contains('article-delete')) {
//            log('点到了 完成按钮, id 是', self.dataset.id)
//            var article_id = self.dataset.id
//            apiArticleDelete(article_id, function(response) {
//                // 删除 self 的父节点
//                self.parentElement.remove()
//            })
//        } else {
//            log('点到了 article cell')
//        }
//    })
//}

//var bindEventArticleEdit = function() {
//    var articleList = e('.article-list')
//    articleList.addEventListener('click', function (event) {
//        var self = event.target
//        if (self.classList.contains('article-edit')) {
//            log('点到了编辑按钮')
//            var article_id = self.dataset.id
//            var t = articleUpdateTemplate(article_id)
//            self.parentElement.insertAdjacentHTML('beforeend', t)
//        } else {
//            log('点到了 article cell')
//        }
//    })
//}
//
//var bindEventArticleUpdate = function() {
//    var articleList = e('.container')
//    articleList.addEventListener('click', function (event) {
//        var self = event.target
//        if (self.classList.contains('wysiwyg-btn')) {
//            log('点到了更新按钮')
//
//            var articleCell = self.closest('.article-cell')
//            var input = articleCell.querySelector('.article-update-input')
//            var articleId = self.dataset.id
//            var form = {
//                id: articleId,
//                task: input.value,
//            }
//            apiArticleUpdate(form, function(response) {
//                log('update', response)
//
//                var updateForm = articleCell.querySelector('.article-update-form')
//                updateForm.remove()
//
//                var article = JSON.parse(response)
//                var articleTag = articleCell.querySelector('.article-task')
//                articleTag.innerHTML = article.task
//            })
//
//        } else {
//            log('点到了 article cell')
//        }
//    })
//}


var bindEvents = function() {



    bindEventArticleAdd()
}

var __main = function() {
    bindEvents()
}

__main()






/*
给 删除 按钮绑定删除的事件
1, 绑定事件
2, 删除整个 article-cell 元素
*/
// var articleList = e('.article-list')
// // 事件响应函数会被传入一个参数, 就是事件本身
// articleList.addEventListener('click', function(event){
//     // log('click articlelist', event)
//     // 我们可以通过 event.target 来得到被点击的元素
//     var self = event.target
//     // log('被点击的元素是', self)
//     // 通过比较被点击元素的 class 来判断元素是否是我们想要的
//     // classList 属性保存了元素的所有 class
//     // 在 HTML 中, 一个元素可以有多个 class, 用空格分开
//     // log(self.classList)
//     // 判断是否拥有某个 class 的方法如下
//     if (self.classList.contains('article-delete')) {
//         log('点到了 删除按钮')
//         // 删除 self 的父节点
//         // parentElement 可以访问到元素的父节点
//         self.parentElement.remove()
//     } else {
//         // log('点击的不是删除按钮******')
//     }
// })
