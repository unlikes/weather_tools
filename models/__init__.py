# 这里保存了model模块的基本模板,
# 注意, 在引用下列模块时, 会自动引用model.__init__中的内容  所以在测试时要加以注意


import json
import time

from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.automap import automap_base

Base = declarative_base()


class Model_action():

    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"

    @classmethod
    def get_engine(cls):
        m = cls()
        path = m.path
        engine = create_engine(path, echo=True)
        return engine

    @classmethod
    def get_session(cls):
        engine = cls.get_engine()
        Session = sessionmaker(bind=engine, )
        Session.configure(bind=engine, )  # once engine is available
        session = Session()
        return session

    def save(self):
        session = self.get_session()
        # models = self.all()
        # if len(models) == 0:
        #     self.id = 1
        # else:
        #     m = models[-1]
        #     self.id = m.id + 1
        session.add(self)
        session.commit()
        session.close()

    @classmethod
    def creat_sheet(cls):
        m = cls()
        Base = m.base
        Engine = cls.get_engine()
        Base.metadata.create_all(Engine)
        return cls

    @classmethod
    def new(cls, form, **kwargs):
        m = cls()
        for k, v in form.items():
            if k in cls.__dict__:
                setattr(m, k, v)

        for k, v in kwargs.items():
            # print(k)
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        m.save()

    @classmethod
    def all(cls):
        """
        all 方法(类里面的函数叫方法)使用 load 函数得到所有的 models
        """
        session = cls.get_session()
        models = []
        for instance in session.query(cls):
            models.append(instance)
        return models

    @classmethod
    def lens(cls):
        session = cls.get_session()
        models = []
        for instance in session.query(cls):
            models.append(instance)
        legth = len(models)
        return legth

    @classmethod
    def find_by_mix(cls, **kwargs):
        session = cls.get_session()
        models = []
        for instance in session.query(cls).order_by(cls.id).filter_by(**kwargs):
            models.append(instance)
        return models

    @classmethod
    def find_by(cls, **kwargs):
        """
        用法如下，kwargs 是只有一个元素的 dict
        u = User.find_by(username='gua')
        """
        session = cls.get_session()
        models = []
        for instance in session.query(cls).order_by(cls.id).filter_by(**kwargs):
            models.append(instance)
        return models

    @classmethod
    def find_like(cls,**kwargs):
        session = cls.get_session()
        models = []
        for instance in session.query(cls).order_by(cls.id).filter_by(**kwargs):
            models.append(instance)
        return models


    @classmethod
    def update_all(cls, form_want, **kwargs):
        session = cls.get_session()
        findlist = cls.find_by_mix(**kwargs)
        for m in findlist:
            for k,v in form_want.items():
                if k in cls.__dict__:
                    setattr(m, k, v)
                else:
                    raise KeyError
            session.commit()
        session.close()

    @classmethod
    def find(cls, id):
        session = cls.get_session()
        model = session.query(cls).order_by(cls.id).filter_by(id=id).first()
        return model



    @classmethod
    def update(cls, form, id):
        session = cls.get_session()
        mlist = session.query(cls).filter_by(id=id)
        for m in mlist:
            for k, v in form.items():
                if k in dir(cls):
                    setattr(m, k, v)
                    print(m)
                else:
                    raise KeyError
        session.commit()
        session.close()



    @classmethod
    def get(cls, id):
        return cls.find(id=id)

    @classmethod
    def delete(cls, id):
        session = cls.get_session()
        x = session.query(cls).filter(cls.id == id).first()
        if x is not None:
            print('==========xxx=', x)
            session.delete(x)
            session.commit()
            session.close()

    def __repr__(self):
        """
        __repr__ 是一个魔法方法
        简单来说, 它的作用是得到类的 字符串表达 形式
        比如 print(u) 实际上是 print(u.__repr__())
        """
        classname = self.__class__.__name__
        properties = ['{}: ({})'.format(k, v) for k, v in self.__dict__.items()]
        s = '\n'.join(properties)
        return '< {}\n{} \n>\n'.format(classname, s)

    def json(self):
        """
        返回当前 model 的字典表示
        """
        # copy 会复制一份新数据并返回
        d = self.__dict__.copy()
        return d


    @classmethod
    def find_word_in_article(cls,attr,str):
        """用于查询单个次,att是属性名 类似标题或者内容,"""
        session = cls.get_session()
        models = []
        #获得属性
        m = getattr(cls,attr)
        for instance in session.query(cls).filter(m.like('%{}%'.format(str))):
            models.append(instance)
        return models

    @classmethod
    def find_words_in_article(cls,attr,lis):
        """多个关键词的查询工具"""
        session = cls.get_session()
        models = []
        #获得属性
        m = getattr(cls,attr)
        words = [ '%{}%'.format(w) for w in lis ]
        # print("=========",words)
        rule = and_(*[m.like(w) for w in words])
        # print("=========",*[m.like(w) for w in words])
        models = session.query(cls).filter(rule).all()
        return models


    # @classmethod
    # def find_word_in_article(cls,attr,str):
    #     """用于查询单个次,att是属性名 类似标题或者内容,"""
    #     session = cls.get_session()
    #     models = []
    #     #获得属性
    #     m = getattr(cls,attr)
    #     for instance in session.query(cls).filter(m.like('%{}%'.format(str))):
    #         models.append(instance)
    #     return models



