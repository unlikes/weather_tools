from sqlalchemy import *
from models import Base,Model_action


class User(Base, Model_action):
    """
    Model 是所有 model 的基类
    @classmethod 是一个套路用法
    例如
    user = User()
    user.db_path() 返回 User.txt
    """
    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"
        self.base = Base

    __tablename__ = "User"
    id = Column(Integer, primary_key=True,autoincrement=True)
    user_id = Column(Integer, default=0)
    name = Column(String, default='')
    password = Column(String, default='')
    img = Column(String, default='')
    backup = Column(String, default='')

    #这个是关键的,不能包在外面


