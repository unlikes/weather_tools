from sqlalchemy import *
from models import Base,Model_action
import time

class Article(Base, Model_action):
    """
    Model 是所有 model 的基类
    @classmethod 是一个套路用法
    例如
    user = User()
    user.db_path() 返回 User.txt
    """
    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"
        self.base = Base

    __tablename__ = "Article"
    id = Column(Integer, primary_key=True,autoincrement=True)
    title = Column(String, default='')
    summary = Column(String, default='')
    img = Column(String, default='')
    subtitle = Column(String, default='')
    article = Column(String, default='')
    tags = Column(String, default='')
    #srcid 用于评价分数, 分数高的出现在best5里面
    srcid = Column(Integer, default=0)
    created_time =Column(Integer,default=0)
    updated_time =Column(Integer,default=0)

    @classmethod
    def new(cls, form, **kwargs):
        m = cls()
        for k, v in form.items():
            if k in cls.__dict__:
                setattr(m, k, v)

        for k, v in kwargs.items():
            # print(k)
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        timestamp = int(time.time())
        m.created_time = timestamp
        m.updated_time = timestamp
        m.save()

        @classmethod
        def update(cls, form, id):
            session = cls.get_session()
            m = session.query(cls).filter_by(id=id).first()
            for k, v in form.items():
                if k in dir(cls):
                    setattr(m, k, v)
                    print(m)
                else:
                    raise KeyError
            timestamp = int(time.time())
            m.updated_time = timestamp
            session.commit()
            session.close()


    #这个是关键的,不能包在外面



