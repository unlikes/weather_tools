from sqlalchemy import *
from models import Base,Model_action


class Blog(Base, Model_action):
    """
    Model 是所有 model 的基类
    @classmethod 是一个套路用法
    例如
    user = User()
    user.db_path() 返回 User.txt
    """
    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"
        self.base = Base

    __tablename__ = "Blog"
    id = Column(Integer, primary_key=True,autoincrement=True)
    title = Column(String, default='')
    summary = Column(String, default='')
    img = Column(String, default='')
    subtitle = Column(String, default='')
    article = Column(String, default='')
    tags = Column(String, default='')
    srcid = Column(Integer, default='')

    #这个是关键的,不能包在外面


