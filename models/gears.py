from sqlalchemy import *
from models import Base,Model_action
import time
#科学计算
import scipy as sci
import pandas as pd
#目录操作
import os



class Gears(Base, Model_action):
    """
    Model 是所有 model 的基类
    @classmethod 是一个套路用法
    例如
    user = User()
    user.db_path() 返回 User.txt
    """
    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"
        self.base = Base

    __tablename__ = "Gears"
    id = Column(Integer, primary_key=True,autoincrement=True)
    gear_id = Column(Integer, default=0)
    gear_name = Column(String, default='')
    gear_pic = Column(String, default='')
    gear_url = Column(String, default='')
    gear_class = Column(String, default='')
    tbk_url = Column(String, default='')
    gear_price = Column(Float, default=0.00)
    gear_sales = Column(Integer, default=0)
    tbk_v_return = Column(Float, default=0.00)
    tbk_return = Column(Float, default=0.00)
    gear_saler = Column(String, default="")
    gear_plantform = Column(String, default="")
    coupon_id = Column(String, default="")
    coupon_num = Column(Integer, default=0)
    coupon_remain = Column(Integer, default=0)
    coupon_detail = Column(String, default="")
    coupon_begin = Column(String, default="")
    coupon_end = Column(String, default="")
    coupon_link = Column(String, default="")
    coupon_tbk_link = Column(String, default="")

    created_time =Column(Integer,default=0)
    updated_time =Column(Integer,default=0)

    @classmethod
    def new(cls, form, **kwargs):
        m = cls()
        for k, v in form.items():
            if k in cls.__dict__:
                setattr(m, k, v)

        for k, v in kwargs.items():
            # print(k)
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        timestamp = int(time.time())
        m.created_time = timestamp
        m.updated_time = timestamp
        m.save()

        @classmethod
        def update(cls, form, id):
            session = cls.get_session()
            m = session.query(cls).filter_by(id=id).first()
            for k, v in form.items():
                if k in dir(cls):
                    setattr(m, k, v)
                    print(m)
                else:
                    raise KeyError
            timestamp = int(time.time())
            m.updated_time = timestamp
            session.commit()
            session.close()

    @classmethod
    def get_taobao_data(cls, filepath="data/taobao/taobao-2018-01-02.xls"):
        taobao_data = pd.read_excel(filepath, sheet_name=1)
        # i = 0
        index=taobao_data["商品id"]
        gear_form={}
        m= len(index)
        for x in range(0,m):
            # print(taobao_data.iloc[x,])
            items = taobao_data.iloc[x,]
            gear_form = {
                "gear_id": int(items["商品id"]),
                "gear_name": items["商品名称"],
                "gear_pic": items["商品主图"],
                "gear_url": items["商品详情页链接地址"],
                "gear_class": items["商品一级类目"],
                "tbk_url": items["淘宝客链接"],
                "gear_price": float(items["商品价格(单位：元)"]),
                "gear_sales": int(items["商品月销量"]),
                "tbk_v_return": float(items["收入比率(%)"]),
                "tbk_return": float(items["佣金"]),
                "gear_saler": items["卖家旺旺"],
                "gear_plantform": items["平台类型"],
                "coupon_id": items["优惠券id"],
                "coupon_num": int(items["优惠券总量"]),
                "coupon_remain": int(items["优惠券剩余量"]),
                "coupon_detail": items["优惠券面额"],
                "coupon_begin": items["优惠券开始时间"],
                "coupon_end": items["优惠券结束时间"],
                "coupon_link": items["优惠券链接"],
                "coupon_tbk_link": items["商品优惠券推广链接"],
            }
            cls.creat_sheet()
            if cls.find_words_in_article("gear_id",[gear_form["gear_id"],]) == []:
                cls.new(gear_form)

    @classmethod
    def tags_get(cls):
        m = cls.all()
        lis=[]
        list1=[]
        list2=[]
        list3=[]
        for x in m:
            n = x.gear_class.split("/")
            # print(n)
            try:
                if n[0]:
                    list1.append(n[0])
                if n[1]:
                    list2.append(n[1])
                if n[2]:
                    list3.append(n[2])
            except:
                pass
        list1=list(set(list1))
        list2=list(set(list2))
        list3=list(set(list3))
        print(list1,list2,list3)

















