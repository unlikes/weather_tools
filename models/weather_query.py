from sqlalchemy import *
from models import Base,Model_action
import time

class Weather_query(Base, Model_action):
    """
    Model 是所有 model 的基类
    @classmethod 是一个套路用法
    例如
    user = User()
    user.db_path() 返回 User.txt
    """
    def __init__(self):
        self.path = "sqlite:///data/article.sqlite"
        self.base = Base

    __tablename__ = "WeatherQuery"
    id = Column(Integer, primary_key=True,autoincrement=True)
    query_name = Column(String, default='')
    lon_0 = Column(Integer, default=105)
    lat_0 = Column(Integer, default=35)
    llcrnrlon = Column(Integer, default=70)
    urcrnrlon = Column(Integer, default=140)
    llcrnrlat = Column(Integer, default=16)
    urcrnrlat = Column(Integer, default=55)
    projection = Column(String, default='merc')
    cmp_list = Column(String, default='')
    fh_file_path = Column(String, default='')
    fh_vars_name = Column(String, default='')
    cmp_n = Column(Integer, default=140)
    cmp_gamma = Column(Integer, default=5)
    pic_region = Column(String, default='')
    created_time = Column(Integer,default=0)
    updated_time = Column(Integer,default=0)
    file_savepath = Column(String, default='')


    @classmethod
    def new(cls, form, **kwargs):
        m = cls()
        for k, v in form.items():
            if k in cls.__dict__:
                setattr(m, k, v)

        for k, v in kwargs.items():
            # print(k)
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        timestamp = int(time.time())
        m.created_time = timestamp
        m.updated_time = timestamp
        m.save()

    @classmethod
    def update(cls, form, id):
        session = cls.get_session()
        m = session.query(cls).filter_by(id=id).first()
        for k, v in form.items():
            if k in dir(cls):
                setattr(m, k, v)
                print(m)
            else:
                raise KeyError
        timestamp = int(time.time())
        m.updated_time = timestamp
        session.commit()
        session.close()


    #这个是关键的,不能包在外面

    @classmethod
    def list_tables(cls):
        """
        all 方法(类里面的函数叫方法)使用 load 函数得到所有的 models
        """
        tables = cls.metadata.tables['WeatherQuery']
        namelist = []
        for x in tables.columns:
            namelist.append(x.name)
        return namelist



